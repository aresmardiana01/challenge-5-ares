const express = require('express')
const app = express()
const port = 3001
app.use("/img",express.static('assets'))
app.use(express.static('public'))
app.set('view engine', 'ejs') 


app.get("/", (req, res) => {
    res.render("pages/index");
  });

app.get("/game", (req, res) => {
    res.render("pages/game");
  });

app.get("/user", (req,res) =>{
  const data = require('./data/user.json')
  res.send(data)
})

app.use((req,res) => {
  res.status(404).render("pages/404")
})

app.use((err,req,res,next) =>{
  res.status(500).render("pages/500",{error: err.message})
})

app.listen(port, () => {
    console.log(`Listening on port ${port}`);
  });